# Exempel i turtle

## Kärrtorps Idrottsplats

```turtle
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix spo: <https://www.bbc.co.uk/ontologies/sport/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix org: <http://www.w3.org/ns/org#> .
@prefix locn: <http://www.w3.org/ns/locn#> .

# Kärrtorps Idrottsplats (Main Place)
# This represents the main sports complex that contains various facilities
<https://example.com/place/karrtorps-idrottsplats> a schema:Place ;
    schema:name "Kärrtorps Idrottsplats"@sv, "Kärrtorp Sports Complex"@en ;
    schema:description "Kärrtorps Idrottsplats är en mångsidig idrottsanläggning i södra Stockholm med ishall, tennishall och friidrottsanläggning."@sv ;
    schema:url <https://motionera.stockholm/traning-bad/karrtorps-idrottsplats/> ;
    # Address information
    schema:address [
        a schema:PostalAddress ;
        schema:streetAddress "Fogdevägen 2" ;
        schema:addressLocality "Stockholm" ;
        schema:postalCode "121 55" ;
        schema:addressRegion "Stockholms län" ;
        schema:addressCountry "Sverige"@sv, "Sweden"@en
    ] ;
    # Geographical coordinates
    schema:geo [
        a schema:GeoCoordinates ;
        schema:latitude "59.2838"^^xsd:decimal ;
        schema:longitude "18.1048"^^xsd:decimal ;
        schema:elevation "40"^^xsd:decimal
    ] ;
    # Links to the sports facilities within the complex
    schema:sportsActivityLocation <https://example.com/facility/karrtorps-ishall>,
                                  <https://example.com/facility/karrtorps-tennishall>,
                                  <https://example.com/facility/karrtorps-friidrottsanlaggning> .

# Kärrtorps Ishall (Ice Rink Facility)
<https://example.com/facility/karrtorps-ishall> a schema:SportsActivityLocation ;
    schema:name "Kärrtorps ishall"@sv, "Kärrtorp Ice Rink"@en ;
    schema:description "Inomhusishall med fullstor rink och läktare."@sv ;
    schema:additionalType <https://dataportal.se/concepts/facilitytypes/ice-rink> ;
    # Sports disciplines that can be practiced in the ice rink
    spo:discipline <https://example.com/discipline/ice-hockey>, <https://example.com/discipline/figure-skating> ;
    # Geographical information
    schema:geo [
        a schema:GeoShape ;
        schema:latitude "59.2840"^^xsd:decimal ;
        schema:longitude "18.1050"^^xsd:decimal ;
        schema:polygon "59.2840,18.1050 59.2840,18.1060 59.2835,18.1060 59.2835,18.1050"
    ] ;
    # Opening hours
    schema:openingHoursSpecification [
        a schema:OpeningHoursSpecification ;
        schema:opens "07:00:00"^^xsd:time ;
        schema:closes "23:00:00"^^xsd:time ;
        schema:validFrom "2024-09-24"^^xsd:date ;
        schema:validThrough "2025-04-24"^^xsd:date ;
        schema:dayOfWeek schema:Monday, schema:Tuesday, schema:Wednesday, schema:Thursday, schema:Friday, schema:Saturday, schema:Sunday
    ] ;
    schema:maximumAttendeeCapacity "500"^^xsd:integer ;
    # Amenities available at the ice rink
    schema:amenityFeature [
        a schema:LocationFeatureSpecification ;
        schema:name "Omklädningsrum"@sv, "Changing Rooms"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Parkering"@sv, "Parking"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Cafeteria"@sv, "Cafeteria"@en ;
        schema:value "true"^^xsd:boolean
    ] ;
    # Link to safety information
    foaf:page <https://example.com/safety/ishall-sakerhet> ;
    schema:mainEntityOfPage <https://example.com/Karrtorpsishall> ;
    schema:url <https://example.com/Karrtorpsishall/boka> ;
    # Organizations responsible for the ice rink
    org:organization <https://example.com/org/stockholms-stad>, <https://example.com/org/driftorganisation> ;
    # Specific areas within the ice rink
    locn:location [
        a <http://www.w3.org/ns/prov#Location> ;
        schema:name "Isrink"@sv, "Ice Rink"@en ;
        dcterms:medium <https://example.com/material/ice> ;
        xsd:boolean "true"^^xsd:boolean ; # Indoor
        spo:discipline <https://example.com/discipline/ice-hockey>, <https://example.com/discipline/figure-skating> ;
        schema:additionalType <https://dataportal.se/concepts/facilitytypes/ice-rink> ;
        schema:geo [
            a schema:GeoShape ;
            schema:latitude "59.2840"^^xsd:decimal ;
            schema:longitude "18.1050"^^xsd:decimal ;
            schema:polygon "59.2840,18.1050 59.2840,18.1060 59.2835,18.1060 59.2835,18.1050"
        ]
    ] .

# Kärrtorps Tennishall (Tennis Hall Facility)
<https://example.com/facility/karrtorps-tennishall> a schema:SportsActivityLocation ;
    schema:name "Kärrtorps tennishall"@sv, "Kärrtorp Tennis Hall"@en ;
    schema:description "Inomhustennishall med fyra banor och modern utrustning."@sv ;
    schema:additionalType <https://dataportal.se/concepts/facilitytypes/tennis-hall> ;
    spo:discipline <https://example.com/discipline/tennis> ;
    # Geographical information
    schema:geo [
        a schema:GeoShape ;
        schema:latitude "59.2836"^^xsd:decimal ;
        schema:longitude "18.1046"^^xsd:decimal ;
        schema:polygon "59.2836,18.1046 59.2836,18.1056 59.2831,18.1056 59.2831,18.1046"
    ] ;
    # Opening hours
    schema:openingHoursSpecification [
        a schema:OpeningHoursSpecification ;
        schema:opens "06:00:00"^^xsd:time ;
        schema:closes "22:00:00"^^xsd:time ;
        schema:validFrom "2024-09-24"^^xsd:date ;
        schema:validThrough "2025-09-24"^^xsd:date ;
        schema:dayOfWeek schema:Monday, schema:Tuesday, schema:Wednesday, schema:Thursday, schema:Friday, schema:Saturday, schema:Sunday
    ] ;
    schema:maximumAttendeeCapacity "100"^^xsd:integer ;
    # Amenities available at the tennis hall
    schema:amenityFeature [
        a schema:LocationFeatureSpecification ;
        schema:name "Omklädningsrum"@sv, "Changing Rooms"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Dusch"@sv, "Shower"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Tennisshop"@sv, "Tennis Shop"@en ;
        schema:value "true"^^xsd:boolean
    ] ;
    # Link to safety information
    foaf:page <https://example.com/safety/tennishall-sakerhet> ;
    schema:mainEntityOfPage <https://example.com/Karrtorpstennishall> ;
    schema:url <https://example.com/Karrtorpstennishall/boka> ;
    # Organizations responsible for the tennis hall
    org:organization <https://example.com/org/stockholms-stad>, <https://example.com/org/driftorganisation> ;
    # Specific areas within the tennis hall
    locn:location [
        a <http://www.w3.org/ns/prov#Location> ;
        schema:name "Tennisbana 1"@sv, "Tennis Court 1"@en ;
        dcterms:medium <https://example.com/material/hardcourt> ;
        xsd:boolean "true"^^xsd:boolean ; # Indoor
        spo:discipline <https://example.com/discipline/tennis> ;
        schema:additionalType <https://dataportal.se/concepts/facilitytypes/tennis-court> ;
        schema:geo [
            a schema:GeoShape ;
            schema:latitude "59.2836"^^xsd:decimal ;
            schema:longitude "18.1046"^^xsd:decimal ;
            schema:polygon "59.2836,18.1046 59.2836,18.1048 59.2834,18.1048 59.2834,18.1046"
        ]
    ] .
<https://example.com/facility/karrtorps-friidrottsanlaggning> a schema:SportsActivityLocation ;
    schema:name "Kärrtorps friidrottsanläggning"@sv, "Kärrtorp Athletics Facility"@en ;
    schema:description "Utomhus friidrottsarena med löparbanor och utrustning för hopp- och kastgrenar."@sv ;
    schema:additionalType <https://dataportal.se/concepts/facilitytypes/athletics-facility> ;
    spo:discipline <https://example.com/discipline/athletics> ;
    # Geographical information
    schema:geo [
        a schema:GeoShape ;
        schema:latitude "59.2834"^^xsd:decimal ;
        schema:longitude "18.1052"^^xsd:decimal ;
        schema:polygon "59.2834,18.1052 59.2834,18.1072 59.2829,18.1072 59.2829,18.1052"
    ] ;
    # Opening hours (seasonal)
    schema:openingHoursSpecification [
        a schema:OpeningHoursSpecification ;
        schema:opens "08:00:00"^^xsd:time ;
        schema:closes "21:00:00"^^xsd:time ;
        schema:validFrom "2024-04-24"^^xsd:date ;
        schema:validThrough "2024-10-24"^^xsd:date ;
        schema:dayOfWeek schema:Monday, schema:Tuesday, schema:Wednesday, schema:Thursday, schema:Friday, schema:Saturday, schema:Sunday
    ] ;
    schema:maximumAttendeeCapacity "300"^^xsd:integer ;
    # Amenities available at the athletics facility
    schema:amenityFeature [
        a schema:LocationFeatureSpecification ;
        schema:name "Omklädningsrum"@sv, "Changing Rooms"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Läktare"@sv, "Spectator Stands"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Förråd för utrustning"@sv, "Equipment Storage"@en ;
        schema:value "true"^^xsd:boolean
    ] ;
    # Link to safety information
    foaf:page <https://example.com/safety/friidrott-sakerhet> ;
    schema:mainEntityOfPage <https://example.com/Karrtorpsfriidrott> ;
    schema:url <https://example.com/Karrtorpsfriidrott/boka> ;
    # Organizations responsible for the athletics facility
    org:organization <https://example.com/org/stockholms-stad>, <https://example.com/org/driftorganisation> ;
    # Specific areas within the athletics facility
    locn:location [
        a <http://www.w3.org/ns/prov#Location> ;
        schema:name "Löparbana"@sv, "Running Track"@en ;
        dcterms:medium <https://example.com/material/tartan> ;
        xsd:boolean "false"^^xsd:boolean ; # Outdoor
        spo:discipline <https://example.com/discipline/running> ;
        schema:additionalType <https://dataportal.se/concepts/facilitytypes/running-track> ;
        schema:geo [
            a schema:GeoShape ;
            schema:latitude "59.2834"^^xsd:decimal ;
            schema:longitude "18.1052"^^xsd:decimal ;
            schema:polygon "59.2834,18.1052 59.2834,18.1072 59.2829,18.1072 59.2829,18.1052"
        ]
    ], [
        a <http://www.w3.org/ns/prov#Location> ;
        schema:name "Hoppgrop"@sv, "Long Jump Pit"@en ;
        dcterms:medium <https://example.com/material/sand> ;
        xsd:boolean "false"^^xsd:boolean ; # Outdoor
        spo:discipline <https://example.com/discipline/long-jump>, <https://example.com/discipline/triple-jump> ;
        schema:additionalType <https://dataportal.se/concepts/facilitytypes/jumping-pit> ;
        schema:geo [
            a schema:GeoShape ;
            schema:latitude "59.2833"^^xsd:decimal ;
            schema:longitude "18.1053"^^xsd:decimal ;
            schema:polygon "59.2833,18.1053 59.2833,18.1055 59.2832,18.1055 59.2832,18.1053"
        ]
    ] .# Organizations
# Stockholm City, responsible for the facilities
<https://example.com/org/stockholms-stad> a schema:Organization ;
    org:classification <https://dataportal.se/concepts/organizationclassifcation/Facility> ;
    schema:legalName "Stockholms stad, Idrottsförvaltningen" ;
    schema:url <https://start.stockholm/om-stockholms-stad/organisation/fackforvaltningar/idrottsforvaltningen/> ;
    schema:address [
        a schema:PostalAddress ;
        schema:streetAddress "Löfströms Allé 5" ;
        schema:addressLocality "Sundbyberg" ;
        schema:postalCode "172 66" ;
        schema:addressRegion "Stockholms län" ;
        schema:addressCountry "Sverige"@sv, "Sweden"@en
    ] .

# Operational organization
<https://example.com/org/driftorganisation> a schema:Organization ;
    org:classification <https://dataportal.se/concepts/organizationclassifcation/Operational> ;
    schema:legalName "Driftorganisation" ;
    schema:url <https://operational.com/about-us> ;
    schema:address [
        a schema:PostalAddress ;
        schema:streetAddress "Klockarevägen 2" ;
        schema:addressLocality "Stockholm" ;
        schema:postalCode "116 32" ;
        schema:addressRegion "Stockholms län" ;
        schema:addressCountry "Sverige"@sv, "Sweden"@en
    ] .

# Safety Documents
# Ice Rink Safety Document
<https://example.com/safety/ishall-sakerhet> a dcterms:Policy ;
    dcterms:hasPart [
        a foaf:Document ;
        dcterms:title "Säkerhetsregler för Kärrtorps Ishall"@sv, "Safety Rules for Kärrtorp Ice Rink"@en ;
        dcterms:issued "2024-09-01T00:00:00Z"^^xsd:dateTime ;
        foaf:page <https://example.com/safety/ishall-safety-rules>
    ] .

# Tennis Hall Safety Document
<https://example.com/safety/tennishall-sakerhet> a dcterms:Policy ;
    dcterms:hasPart [
        a foaf:Document ;
        dcterms:title "Säkerhetsregler för Kärrtorps Tennishall"@sv, "Safety Rules for Kärrtorp Tennis Hall"@en ;
        dcterms:issued "2024-09-01T00:00:00Z"^^xsd:dateTime ;
        foaf:page <https://example.com/safety/tennishall-safety-rules>
    ] .

# Athletics Facility Safety Document
<https://example.com/safety/friidrott-sakerhet> a dcterms:Policy ;
    dcterms:hasPart [
        a foaf:Document ;
        dcterms:title "Säkerhetsregler för Kärrtorps Friidrottsanläggning"@sv, "Safety Rules for Kärrtorp Athletics Facility"@en ;
        dcterms:issued "2024-09-01T00:00:00Z"^^xsd:dateTime ;
        foaf:page <https://example.com/safety/athletics-safety-rules>
    ] .
```
## Rålambshovsparken

```turtle
# Prefixes
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix spo: <https://www.bbc.co.uk/ontologies/sport/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix org: <http://www.w3.org/ns/org#> .
@prefix locn: <http://www.w3.org/ns/locn#> .

# Rålambshovsparken (Main Place)
# This represents the main park that contains the skatepark
<https://example.com/place/ralambshovsparken> a schema:Place ;
    schema:name "Rålambshovsparken"@sv, "Rålambshovsparken"@en ;
    schema:description "Rålambshovsparken är en stor park på Kungsholmen i Stockholm, känd för sin skatepark, lekplatser och öppna ytor för rekreation."@sv ;
    schema:url <https://parker.stockholm/parker/ralambshovsparken/> ;
    # Address information
    schema:address [
        a schema:PostalAddress ;
        schema:addressLocality "Stockholm" ;
        schema:postalCode "112 35" ;
        schema:addressRegion "Stockholms län" ;
        schema:addressCountry "Sverige"@sv, "Sweden"@en
    ] ;
    # Geographical coordinates
    schema:geo [
        a schema:GeoCoordinates ;
        schema:latitude "59.3269"^^xsd:decimal ;
        schema:longitude "18.0308"^^xsd:decimal ;
        schema:elevation "5"^^xsd:decimal
    ] ;
    # Link to the sports facility (skatepark) within the park
    schema:sportsActivityLocation <https://example.com/facility/ralambshovsparken-skatepark> .

# Rålambshovsparkens Skatepark (Sports Facility)
# This represents the skatepark itself, which is a sports activity location within Rålambshovsparken
<https://example.com/facility/ralambshovsparken-skatepark> a schema:SportsActivityLocation ;
    schema:name "Rålambshovsparkens Skatepark"@sv, "Rålambshovsparken Skatepark"@en ;
    schema:description "En av Stockholms största och mest populära skateparker, designad för både nybörjare och erfarna åkare."@sv ;
    schema:additionalType <https://dataportal.se/concepts/facilitytypes/skatepark> ;
    # Sports disciplines that can be practiced in the skatepark
    spo:discipline <https://example.com/discipline/skateboarding>, <https://example.com/discipline/inline-skating>, <https://example.com/discipline/bmx> ;
    # Geographical shape of the skatepark
    schema:geo [
        a schema:GeoShape ;
        schema:latitude "59.3267"^^xsd:decimal ;
        schema:longitude "18.0305"^^xsd:decimal ;
        schema:polygon "59.3269,18.0303 59.3269,18.0307 59.3265,18.0307 59.3265,18.0303"
    ] ;
    # Opening hours (in this case, open 24/7)
    schema:openingHoursSpecification [
        a schema:OpeningHoursSpecification ;
        schema:opens "00:00:00"^^xsd:time ;
        schema:closes "23:59:59"^^xsd:time ;
        schema:validFrom "2024-09-24"^^xsd:date ;
        schema:validThrough "2025-09-24"^^xsd:date ;
        schema:dayOfWeek schema:Monday, schema:Tuesday, schema:Wednesday, schema:Thursday, schema:Friday, schema:Saturday, schema:Sunday
    ] ;
    schema:maximumAttendeeCapacity "100"^^xsd:integer ;
    # Amenities available at the skatepark
    schema:amenityFeature [
        a schema:LocationFeatureSpecification ;
        schema:name "Belysning"@sv, "Lighting"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Sittplatser"@sv, "Seating"@en ;
        schema:value "true"^^xsd:boolean
    ], [
        a schema:LocationFeatureSpecification ;
        schema:name "Toaletter"@sv, "Restrooms"@en ;
        schema:value "true"^^xsd:boolean
    ] ;
    # Link to safety information
    foaf:page <https://example.com/safety/skatepark-safety> ;
    schema:mainEntityOfPage <https://example.com/Ralambshovsparkenskatepark> ;
    schema:url <https://example.com/Ralambshovsparkenskatepark/boka> ;
    # Detailed spatial information
    dcterms:spatial [
        a dcterms:Location ;
        dcat:bbox "POLYGON((18.0303 59.3269, 18.0307 59.3269, 18.0307 59.3265, 18.0303 59.3265, 18.0303 59.3269))"^^<http://www.opengis.net/ont/geosparql#wktLiteral> ;
        locn:geometry "POLYGON((18.0303 59.3269, 18.0307 59.3269, 18.0307 59.3265, 18.0303 59.3265, 18.0303 59.3269))"
    ] ;
    # Organizations responsible for the skatepark
    org:organization <https://example.com/org/stockholms-stad>, <https://example.com/org/parkdriftsenheten> ;
    # Specific areas within the skatepark
    locn:location [
        a <http://www.w3.org/ns/prov#Location> ;
        schema:name "Huvudskateyta"@sv, "Main skate area"@en ;
        dcterms:medium <https://example.com/material/concrete> ;
        xsd:boolean "false"^^xsd:boolean ; # Outdoor
        spo:discipline <https://example.com/discipline/skateboarding>, <https://example.com/discipline/inline-skating>, <https://example.com/discipline/bmx> ;
        schema:additionalType <https://dataportal.se/concepts/facilitytypes/skatepark> ;
        schema:geo [
            a schema:GeoShape ;
            schema:latitude "59.3268"^^xsd:decimal ;
            schema:longitude "18.0304"^^xsd:decimal ;
            schema:polygon "59.3268,18.0304 59.3268,18.0306 59.3266,18.0306 59.3266,18.0304"
        ]
    ], [
        a <http://www.w3.org/ns/prov#Location> ;
        schema:name "Bowl-area"@sv, "Bowl area"@en ;
        dcterms:medium <https://example.com/material/concrete> ;
        xsd:boolean "false"^^xsd:boolean ; # Outdoor
        spo:discipline <https://example.com/discipline/skateboarding>, <https://example.com/discipline/bmx> ;
        schema:additionalType <https://dataportal.se/concepts/facilitytypes/skate-bowl> ;
        schema:geo [
            a schema:GeoShape ;
            schema:latitude "59.3267"^^xsd:decimal ;
            schema:longitude "18.0303"^^xsd:decimal ;
            schema:polygon "59.3267,18.0303 59.3267,18.0305 59.3265,18.0305 59.3265,18.0303"
        ]
    ] .

# Organizations
# Stockholm City, responsible for the facility
<https://example.com/org/stockholms-stad> a schema:Organization ;
    org:classification <https://dataportal.se/concepts/organizationclassifcation/Facility> ;
    schema:legalName "Stockholms stad, Idrottsförvaltningen" ;
    schema:url <https://start.stockholm/om-stockholms-stad/organisation/fackforvaltningar/idrottsforvaltningen/> ;
    schema:address [
        a schema:PostalAddress ;
        schema:addressLocality "Sundbyberg" ;
        schema:postalCode "172 66" ;
        schema:addressRegion "Stockholms län" ;
        schema:addressCountry "Sverige"@sv, "Sweden"@en
    ] .

# Park Operations Unit, responsible for operational management
<https://example.com/org/parkdriftsenheten> a schema:Organization ;
    org:classification <https://dataportal.se/concepts/organizationclassifcation/Operational> ;
    schema:legalName "Parkdriftsenheten" ;
    schema:url <https://start.stockholm/om-stockholms-stad/organisation/fackforvaltningar/trafikkontoret/> ;
    schema:address [
        a schema:PostalAddress ;
        schema:addressLocality "Stockholm" ;
        schema:postalCode "112 26" ;
        schema:addressRegion "Stockholms län" ;
        schema:addressCountry "Sverige"@sv, "Sweden"@en
    ] .

# Safety Document
# This represents the safety policy and rules for the skatepark
<https://example.com/safety/skatepark-safety> a dcterms:Policy ;
    dcterms:hasPart [
        a foaf:Document ;
        dcterms:title "Säkerhetsregler för Rålambshovsparkens Skatepark"@sv, "Safety Rules for Rålambshovsparken Skatepark"@en ;
        dcterms:issued "2024-09-01T00:00:00Z"^^xsd:dateTime ;
        foaf:page <https://example.com/safety/skatepark-safety-rules>
    ] .
```