## Naming conventions for terminologies

This is a summary of the three core terminologies used in the information model and their naming conventions:

facility-type - Physical sports facilities/venues
activity-type - Sports and activities performed
surface-type - Surface/material types used

This forms a comprehensive classification system for Swedish sports facilities, linking physical locations with their activities and surface materials. 

### Facility Types (Anläggningstyper) - /facility-type/{code}


Format: Four digit codes starting from 1000-5700
Examples:

1000: Badmintonanläggning
2000: Bågskytteanläggning
3000: Gymanläggning
4000: Motionsspår
5000: Skyttesportanläggning

For subconcepts you keep the first two digits: 

Motorsportanläggning: 4100 
Flygsportanläggning: 4104 



### Activity Types (Typer av aktiviteter) - /activity-type/{code}


Format: Four digit codes starting from 1050-6950, increments of 50
Examples:

- 1050: Aerobics
- 2550: Dans
- 3450: Gymnastik
- 4850: Paraishockey
- 6950: Övrig motionsaktivitet




### Surface Types (Typer av underlag) - /surface-type/{code}


Format: Three digit codes starting from 100, increments of 5
Examples:

- 100: Annat underlag
- 105: Asfalt
- 150: Gummi
- 200: Naturlig terräng
- 250: Vatten