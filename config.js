rdforms_specs.init({
    language: document.targetLanguage,
    namespaces: {
    },
    bundles: [
      ['../model1.0.json'],
    ],
    main: [
      'Sportsfacilities-12',
      'Sportsfacilities-37'
    ],
    supportive: [      
        'Sportsfacilities-107',
        'Sportsfacilities-134',
        'Sportsfacilities-135',
        'Sportsfacilities-21',
        //'Sportsfacilities-146',
        //'Sportsfacilities-160',
        'Sportsfacilities-87',
        'Sportsfacilities-45',
        'Sportsfacilities-91',
        'Sportsfacilities-70',
        'Sportsfacilities-69',
        'Sportsfacilities-71',
        'Sportsfacilities-66' 
    ]
  });
