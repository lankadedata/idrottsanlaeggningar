# Sports and Outdoor Sports Facilities


## Getting started

This repository is the source code for the Swedish national application profile for Sports and Outdoor Sports facilities. 

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/lankadedata/saf.git
git branch -M main
git push -uf origin main
```

## Clone this repository

```
git clone https://gitlab.com/lankadedata/saf.git
cd saf
``` 

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/lankadedata/idrottsanlaeggningar/-/settings/integrations)


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Roadmap

- Updating terminologies with new concepts  

## Contributing

Create an issue and refer to what should be added or changed in the model using [`Class`](https://example.org/Class) or [`property`](https://example.org/property) references. 

If an issue is accepted and deployed this will be acknowledged via a reference on the specification page. 


## License

The license is described in [LICENSE.md](#LICENSE.md)

