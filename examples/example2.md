# Hur vi beskriver olika idrottsplatser enligt föreslagen informationsmodell:

## Rålambshovsparken  

- En plats med flertalet anläggningar samt aktivitetsytor där vi i huvudsak fokuserar på skateparken. Detta exempel har syftet att visa hur även friluftlivet kan kopplas ihop med denna plats i ett senare skede.

I informationsmodellen beskrivs det här: [Plats](https://lankadedata.se/spec/sport/sv/#schema%3APlace)

- **Namn:** Rålambshovsparken
- **Anläggning:** [https://example.com/RalambshovsparkensSkatepark](https://example.com/RalambshovsparkensSkatepark)
- **Beskrivning:** Rålambshovsparken är en stor park på Kungsholmen i Stockholm, känd för sin skatepark, lekplatser och öppna ytor för rekreation.
- **Webbplats:** [https://parker.stockholm/parker/ralambshovsparken/](https://parker.stockholm/parker/ralambshovsparken/)
- **Adress:** 
  - Gatuadress: Smedsuddsvägen
  - Postnummer: 112 35
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige
- **Koordinater:** 
  - Latitud: 59.3269
  - Longitud: 18.0308
- **Elevation:** 5 meter

### Anläggningar inom Rålambshovsparken

#### 1. Skatepark

- **Namn:** Rålambshovsparkens Skatepark
- **Anläggningstyp:** Skatepark
- **Aktivitet:** Skateboard, Inlines, BMX
- **Beskrivning:** En av Stockholms största och mest populära skateparker, designad för både nybörjare och erfarna åkare.
- **Koordinater:** 
  - Latitud: 59.3267
  - Longitud: 18.0305
- **Geografiskt område:** Polygon: 59.3269,18.0303 59.3269,18.0307 59.3265,18.0307 59.3265,18.0303
- **Öppettider:**
  - Öppnar: 00:00
  - Stänger: 23:59
  - Giltigt från: 20240924
  - Giltigt till: 20250924
  - Veckodag: Måndag, Tisdag, Onsdag, Torsdag, Fredag, Lördag, Söndag
- **Kapacitet:** 100
- **Bekvämligheter:** Belysning, sittplatser, närliggande toaletter
- **Säkerhetsdokument:** https://example.com/skatepark-sakerhet
- **Ingångssida:** https://example.com/Ralambshovsparkenskatepark
- **Organisation:** https://stockholmsstad.se
                    https://operational.com/
- **Yta:** https://stockholmsstad.se/skateyta
- **Yta:** https://stockholmsstad.se/bowlarea

#### 1.1 Aktivitetsytor inom skateparken

- **Namn:** Huvudskateyta
- **Underlag:** Betong
- **Placering:** Utomhus
- **Aktivitet:** Skateboard, Inlines, BMX
- **Anläggningstyp:** Skatepark
- **Säsong:**
  - Giltigt från: 20240924
  - Giltigt till: 20250924
- **Yta:** Polygon: 59.3268,18.0304 59.3268,18.0306 59.3266,18.0306 59.3266,18.0304

- **Namn:** Bowl-area
- **Underlag:** Betong
- **Placering:** Utomhus
- **Aktivitet:** Skateboard, BMX
- **Anläggningstyp:** Skate bowl
- **Säsong:**
  - Giltigt från: 20240924
  - Giltigt till: 20250924
- **Yta:** Polygon: 59.3267,18.0303 59.3267,18.0305 59.3265,18.0305 59.3265,18.0303

#### Organization

- **Ansvarig för:** https://dataportal.se/concepts/organizationclassifcation/Facility
- **Organisationsnamn:** Stockholms stad, Idrottsförvaltningen
- **Hemsida:** https://start.stockholm/om-stockholms-stad/organisation/fackforvaltningar/idrottsforvaltningen/
- **Adress:** 
  - Gatuadress: Löfströms Allé 5
  - Postnummer: 172 66
  - Ort: Sundbyberg
  - Region: Stockholms län
  - Land: Sverige

- **Ansvarig för:** https://dataportal.se/concepts/organizationclassifcation/Operational
- **Organisationsnamn:** Parkdriftsenheten
- **Hemsida:** https://start.stockholm/om-stockholms-stad/organisation/fackforvaltningar/trafikkontoret/
- **Adress:** 
  - Gatuadress: Fleminggatan 4
  - Postnummer: 112 26
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige