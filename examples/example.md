# Hur vi beskriver olika idrottsplatser enligt föreslagen informationsmodell:

## Kärrtorps Idrottsplats 

- En plats med flertalet anläggningar samt aktivitetsytor 

### Huvudplats

I informationsmodellen beskrivs det här: [Plats](https://lankadedata.se/spec/sport/sv/#schema%3APlace)

- **Namn:** Kärrtorps Idrottsplats
- [**Anläggning:**](#2-tennishall) [https://example.com/SparvagensTK](https://example.com/SparvagensTK)
- [**Anläggning:**](#1-fotbollsanläggning) [https://example.com/KarrtorpsFotbollsplan](https://example.com/KarrtorpsFotbollsplan)
- [**Anläggning:**](#3-friidrottsanläggning) [https://example.com/KarrtorpsFriidrottsanläggning](https://example.com/KarrtorpsFriidrottsanläggning)
- [**Anläggning:**](#1-ishall) [https://example.com/KarrtorpsIshall](https://example.com/KarrtorpsIshall)
- **Beskrivning:** Kärrtorps Idrottsplats är en mångsidig idrottsanläggning i södra Stockholm med fotbollsplan, friidrottsbana, tennisbanor och ishall.
- **Webbplats:** [https://motionera.stockholm/traning-bad/karrtorps-idrottsplats/](https://motionera.stockholm/traning-bad/karrtorps-idrottsplats/)
- **Adress:** 
  - Gatuadress: Fogdevägen 2
  - Postnummer: 121 55
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige
- **Koordinater:** 
  - Latitud: 59.2838
  - Longitud: 18.1048
- **Elevation:**  40 meter


### Anläggningar inom Kärrtorps Idrottsplats

#### 1. Fotbollsanläggning

- **Namn:** Kärrtorps fotbollsplan
- **Anläggningstyp:** Fotbollsplan
- **Aktivitet:** Fotboll
- **Beskrivning:** 11-spelsplaner för fotboll med konstgräs och belysning.
- **Koordinater:** 
  - Latitud: 59.2838
  - Longitud: 18.1048
  - **Geografiskt område:** Polygon: 59.2838,18.1048 59.2838,18.1068 59.2833,18.1068 59.2833,18.1048
  - **Öppettider:**
  - Öppnar: 08:00
  - Stänger: 22:00
  - Giltigt från: 20240924
  - Giltigt till: 20241224
  - Veckodag: Fredag, Lördag, Söndag
- **Kapacitet:** 200 
- **Bekvämligheter:** Omklädningsrum, parkering, belysning
- **Säkerhetsdokument:** https://example.com/brandrutin
- **Ingångssida:** https://example.com/Karrtorpsfotbollplan
- **Bokningssida:** https://example.com/Karrtorpsfotbollplan/boka
- **Organisation:** https://stockholmsstad.se
- https://operational.com/
- **Yta:** https://stockholmsstad.se/plan1
- **Yta:** https://stockholmsstad.se/plan2
- **Yta:** https://stockholmsstad.se/plan3

#### 1.1 Aktivitetsytor inom fotbollsanläggningen

- **Namn:** Plan 1
- **Underlag:** Konstgräs
- **Placering:** Utomhus
- **Aktivitet:** Fotboll, Amerikansk fotboll
- **Anläggningstyp:** 11-spelsplan 
- **Säsong:**
- Giltigt från: 20240924
- Giltigt till: 20241124
- **Yta:** Polygon: 59.2838,18.1048 59.2838,18.1068 59.2833,18.1068 59.2833,18.1048

- **Namn:** Plan 2
- **Underlag:** Konstgräs
- **Placering:** Utomhus
- **Aktivitet:** Fotboll, Rugby
- **Anläggningstyp:** 11-spelsplan 
- **Säsong:**
- Giltigt från: 20240924
- Giltigt till: 20241124
- **Yta:** Polygon: 59.2838,18.1048 59.2838,18.1068 59.2833,18.1068 59.2833,18.1048

- **Namn:** Plan 3
- **Underlag:** Konstgräs
- **Placering:** Utomhus
- **Aktivitet:** Fotboll
- **Anläggningstyp:** 11-spelsplan 
- **Säsong:**
- Giltigt från: 20240924
- Giltigt till: 20241124
- **Yta:** Polygon: 59.2838,18.1048 59.2838,18.1068 59.2833,18.1068 59.2833,18.1048

#### 1.3 Organization 

- **Ansvarig för:** https://dataportal.se/concepts/organizationclassifcation/Facility
- **Organisationsnamn:** Anläggningsansvar
- **Hemsida:** https://stockholmsstad.com/about-us
- **Adress:** 
  - Gatuadress: Dravägen 2
  - Postnummer: 114 32
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige

- **Ansvarig för:** https://dataportal.se/concepts/organizationclassifcation/Operational
- **Organisationsnamn:** Driftorganisation
- **Hemsida:** https://operational.com/about-us
- **Adress:** 
  - Gatuadress: Klockarevägen 2
  - Postnummer: 116 32
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige
 


#### 1. Ishall

- **Namn:** Kärrtorps ishall
- **Anläggningstyp:** Ishall
- **Aktivitet:** Ishockey, Konståkning
- **Beskrivning:** Inomhusishall med fullstor rink och läktare.
- **Koordinater:** 
  - Latitud: 59.2840
  - Longitud: 18.1050
- **Geografiskt område:** Polygon: 59.2840,18.1050 59.2840,18.1060 59.2835,18.1060 59.2835,18.1050
- **Öppettider:**
  - Öppnar: 07:00
  - Stänger: 23:00
  - Giltigt från: 20240924
  - Giltigt till: 20250424
  - Veckodag: Måndag, Tisdag, Onsdag, Torsdag, Fredag, Lördag, Söndag
- **Kapacitet:** 500
- **Bekvämligheter:** Omklädningsrum, parkering, cafeteria
- **Säkerhetsdokument:** https://example.com/ishall-sakerhet
- **Ingångssida:** https://example.com/Karrtorpsishall
- **Bokningssida:** https://example.com/Karrtorpsishall/boka
- **Organisation:** https://stockholmsstad.se
- https://operational.com/
- **Yta:** https://stockholmsstad.se/isrink

#### 1.1 Aktivitetsytor inom ishallen

- **Namn:** Isrink
- **Underlag:** Is
- **Placering:** Inomhus
- **Aktivitet:** Ishockey, Konståkning
- **Anläggningstyp:** Fullstor isrink
- **Säsong:**
  - Giltigt från: 20240924
  - Giltigt till: 20250424
- **Yta:** Polygon: 59.2840,18.1050 59.2840,18.1060 59.2835,18.1060 59.2835,18.1050

#### 2. Tennishall

- **Namn:** Kärrtorps tennishall
- **Anläggningstyp:** Tennishall
- **Aktivitet:** Tennis
- **Beskrivning:** Inomhustennishall med fyra banor och modern utrustning.
- **Koordinater:** 
  - Latitud: 59.2836
  - Longitud: 18.1046
- **Geografiskt område:** Polygon: 59.2836,18.1046 59.2836,18.1056 59.2831,18.1056 59.2831,18.1046
- **Öppettider:**
  - Öppnar: 06:00
  - Stänger: 22:00
  - Giltigt från: 20240924
  - Giltigt till: 20250924
  - Veckodag: Måndag, Tisdag, Onsdag, Torsdag, Fredag, Lördag, Söndag
- **Kapacitet:** 100
- **Bekvämligheter:** Omklädningsrum, dusch, tennisshop
- **Säkerhetsdokument:** https://example.com/tennishall-sakerhet
- **Ingångssida:** https://example.com/Karrtorpstennishall
- **Bokningssida:** https://example.com/Karrtorpstennishall/boka
- **Organisation:** https://stockholmsstad.se
- https://operational.com/
- **Yta:** https://stockholmsstad.se/tennisbana1
- **Yta:** https://stockholmsstad.se/tennisbana2
- **Yta:** https://stockholmsstad.se/tennisbana3
- **Yta:** https://stockholmsstad.se/tennisbana4

#### 2.1 Aktivitetsytor inom tennishallen

- **Namn:** Tennisbana 1-4
- **Underlag:** Hardcourt
- **Placering:** Inomhus
- **Aktivitet:** Tennis
- **Anläggningstyp:** Tennisbana
- **Säsong:**
  - Giltigt från: 20240924
  - Giltigt till: 20250924
- **Yta:** Polygon: 59.2836,18.1046 59.2836,18.1056 59.2831,18.1056 59.2831,18.1046

#### 3. Friidrottsanläggning

- **Namn:** Kärrtorps friidrottsanläggning
- **Anläggningstyp:** Friidrottsarena
- **Aktivitet:** Friidrott
- **Beskrivning:** Utomhus friidrottsarena med löparbanor och utrustning för hopp- och kastgrenar.
- **Koordinater:** 
  - Latitud: 59.2834
  - Longitud: 18.1052
- **Geografiskt område:** Polygon: 59.2834,18.1052 59.2834,18.1072 59.2829,18.1072 59.2829,18.1052
- **Öppettider:**
  - Öppnar: 08:00
  - Stänger: 21:00
  - Giltigt från: 20240424
  - Giltigt till: 20241024
  - Veckodag: Måndag, Tisdag, Onsdag, Torsdag, Fredag, Lördag, Söndag
- **Kapacitet:** 300
- **Bekvämligheter:** Omklädningsrum, läktare, förråd för utrustning
- **Säkerhetsdokument:** https://example.com/friidrott-sakerhet
- **Ingångssida:** https://example.com/Karrtorpsfriidrott
- **Bokningssida:** https://example.com/Karrtorpsfriidrott/boka
- **Organisation:** https://stockholmsstad.se
- https://operational.com/
- **Yta:** https://stockholmsstad.se/loparbana
- **Yta:** https://stockholmsstad.se/hoppgrop
- **Yta:** https://stockholmsstad.se/kastyta

#### 3.1 Aktivitetsytor inom friidrottsanläggningen

- **Namn:** Löparbana
- **Underlag:** Tartanbeläggning
- **Placering:** Utomhus
- **Aktivitet:** Löpning
- **Anläggningstyp:** 400m löparbana
- **Säsong:**
  - Giltigt från: 20240424
  - Giltigt till: 20241024
- **Yta:** Polygon: 59.2834,18.1052 59.2834,18.1072 59.2829,18.1072 59.2829,18.1052

- **Namn:** Hoppgrop
- **Underlag:** Sand
- **Placering:** Utomhus
- **Aktivitet:** Längdhopp, Tresteg
- **Anläggningstyp:** Hoppgrop
- **Säsong:**
  - Giltigt från: 20240424
  - Giltigt till: 20241024
- **Yta:** Polygon: 59.2833,18.1053 59.2833,18.1055 59.2832,18.1055 59.2832,18.1053

- **Namn:** Kastyta
- **Underlag:** Gräs
- **Placering:** Utomhus
- **Aktivitet:** Kulstötning, Spjutkastning
- **Anläggningstyp:** Kastyta
- **Säsong:**
  - Giltigt från: 20240424
  - Giltigt till: 20241024
- **Yta:** Polygon: 59.2832,18.1060 59.2832,18.1065 59.2830,18.1065 59.2830,18.1060

#### Organization

- **Ansvarig för:** https://dataportal.se/concepts/organizationclassifcation/Facility
- **Organisationsnamn:** Anläggningsansvar
- **Hemsida:** https://stockholmsstad.com/about-us
- **Adress:** 
  - Gatuadress: Dravägen 2
  - Postnummer: 114 32
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige

- **Ansvarig för:** https://dataportal.se/concepts/organizationclassifcation/Operational
- **Organisationsnamn:** Driftorganisation
- **Hemsida:** https://operational.com/about-us
- **Adress:** 
  - Gatuadress: Klockarevägen 2
  - Postnummer: 116 32
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige






## Eriksdalsbadet

### Huvudplats

- **Namn:** Eriksdalsbadet
- **Beskrivning:** Eriksdalsbadet är ett stort badhus i Stockholm med både inomhus- och utomhusbassänger, samt andra träningsfaciliteter.
- **Adress:**
  - Gatuadress: Hammarby Slussväg 20
  - Postnummer: 118 72
  - Ort: Stockholm
  - Region: Stockholms län
  - Land: Sverige
- **Telefon:** +46 (0)8-508 40 258
- **Webbplats:** [https://motionera.stockholm/traning-bad/eriksdalsbadet/](https://motionera.stockholm/traning-bad/eriksdalsbadet/)
- **Koordinater:**
  - Latitud: 59.3066
  - Longitud: 18.0785

### Anläggningar inom Eriksdalsbadet

#### 1. Simanläggning (inomhus)

- **Namn:** Inomhusbassäng
- **Typ:** Simbassäng
- **Aktivitet:** Simning
- **Beskrivning:** 50-metersbassäng inomhus för simning och vattensporter.
- **Placering:** Inomhus
- **Underlag:** Vatten
- **Maximal kapacitet:** 500 personer
- **Öppettider:**
  - Dagar: Måndag - Söndag
  - Öppnar: 06:00
  - Stänger: 22:00

##### 50-metersbassäng

- **Namn:** 50-metersbassäng
- **Beskrivning:** 50 meter lång bassäng med 8 banor
- **Egenskaper:**
  - Djup: 2.0 meter

#### 2. Simhoppsanläggning

- **Namn:** Simhoppstorn
- **Typ:** Simhoppstorn
- **Aktivitet:** Simhopp
- **Beskrivning:** Simhoppstorn med olika höjder för simhopp.
- **Placering:** Inomhus
- **Egenskaper:**
  - Hopptorn höjder: 1m, 3m, 5m, 7.5m, 10m

#### 3. Utomhusbassäng

- **Namn:** Utomhusbassäng
- **Typ:** Simbassäng
- **Aktivitet:** Simning
- **Beskrivning:** 50-metersbassäng utomhus för simning under sommarsäsongen.
- **Placering:** Utomhus
- **Underlag:** Vatten
- **Maximal kapacitet:** 300 personer
- **Säsongsöppettider:**
  - Gäller från: 2023-05-01
  - Gäller till: 2023-08-31
  - Dagar: Måndag - Söndag
  - Öppnar: 10:00
  - Stänger: 20:00

#### 4. Gym

- **Namn:** Gym
- **Typ:** Gym
- **Aktivitet:** Styrketräning
- **Beskrivning:** Fullt utrustat gym för styrketräning och konditionsträning.
- **Placering:** Inomhus
- **Maximal kapacitet:** 100 personer
- **Öppettider:**
  - Måndag - Fredag:
    - Öppnar: 06:00
    - Stänger: 22:00
  - Lördag - Söndag:
    - Öppnar: 08:00
    - Stänger: 20:00


### Konventioner 

En aktivitetsyte som i angivna exemplen nedan ska följa angivna konventioner: 

- Aktivitet, anläggningstyp, storlek, antal, kategori och flera egenskaper som kan anges

Om vi tar exemplet med en idrottshall där flera aktiviteter kan utföras på samma yta anges det enligt ovan bestämd konvention: 

- fotboll
- idrottshall
- 30 * 20 
- 4 
- 5-spelsplaner 

<div>

**Exempel med annan sport** 

- volleyboll
- idrottshall
- 30 * 40 
- 2 