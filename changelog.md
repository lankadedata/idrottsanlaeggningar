# Changelog 1.0 

### Added 

- Updated README.md version 
- added SHACL file
- added naming conventions for the terminologies 

### Fixed

- Smaller improvements to class and property descriptions

### Changed 

- Changed [`rdfs:label`](http://www.w3.org/2000/01/rdf-schema#label) property to [`dcterms:type`](http://purl.org/dc/terms/type) in [`prov:Activity`](http://www.w3.org/ns/prov#Activity) class. 
- Changed swedish naming of [`schema:PostalAddress`](http://schema.org/PostalAddress)


# Changelog 0.9.9

### Added

- Support for indoor/outdoor specification using boolean property
- Geographic specifications including:

- Centroid coordinates for main geopgraphy of the facility 
- Support for different coordinate reference systems according to GEODCAT-AP 

- ConceptSchemes for specific properties (surface, classification and type of facility/activity area)


- Support for seasonality through specialOpeningHoursSpecification
- Image management with licensing and attribution support
- External reference system through schema:sameAs for connecting to national databases

### Fixed

- Improved geographic representation with both bounding boxes and detailed polygons
- Enhanced organization roles and classifications

### Changed

- Updated structure for activity areas within facilities
- Expanded facility type classifications using controlled vocabulary
- Improved support for surface types and materials documentation
- Enhanced opening hours specification with validity periods
- Changed property schema:additionalType to dcterms:type for types of facilities and activity areas 
- Grouped the roles maintenance and operator 

# Changelog 0.9.2


### Added

- the property foaf:page for policy 
- new examples for skate park and non complex facility

### Fixed

- smaller cardinalities changes

### Changed

- prov:Location is used instead of dcterms:Location for activity areas
 

### Added

- Introduced [`containsPlace`](https://schema.org/containsPlace) to enable hierarchical structure of places
- Concept of sports halls with specific courts/fields (e.g., court A, B, C or volleyball court A, B, C)
- Capability to specify daily opening hours
- Object containing the activity location contained within a [`SportsActivityLocation`](https://schema.org/SportsActivityLocation)

### Fixed

- Improved linking from place to recreational facility and activities (prov:activity)
- Updated organization model to better handle various roles:

  - Operator
  - Maintainer
  - Facility

### Changed

- Updated structure to handle sports facilities more effectively

### To Address

- Develop reading instructions for the model (focusing on "staying on the page")
- ~~ Evaluate the need for object form under facility ~~

### Notes for Future Consideration

- Explore the possibility of further refining the handling of sports facilities and their components
- Consider developing more detailed guidelines for how different organizational roles should be represented in the model
- ~~Strict relation between [`Place`](https://schema.org/Place) and [`SportsActivityLocation`](https://schema.org/SportsActivityLocation), consider the possibility of having a object form that explains the possible uses of the [`SportsActivityLocation`](https://schema.org/SportsActivityLocation)~~
- ~~ [`Space`](https://ontology.brickschema.org/rec/Space.html) for the sub locations contained in the [`SportsActivityLocation`](https://schema.org/SportsActivityLocation) ~~